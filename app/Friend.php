<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    protected $fillable = ['name','email','phone'];
    public function girlfirend(){
        return $this->hasMany('App\girlfirend','boyFriend','name');
    }
}
