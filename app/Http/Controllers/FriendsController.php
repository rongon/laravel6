<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Friend;
use App\girlfirend;

class FriendsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
       $students = Friend::paginate(2);
       return view('admin.pages.index', compact('students'));
   }
    // public function index()
    // {
    //     $data = friend::all();
    //     return view('admin.pages.index',compact('data'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Friend::create($request->all());
        return redirect (route('friends.index'))->with('success','insart successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Friend::with('girlfirend')->find($id);
        return view('admin.pages.read', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Friend::find($id);
        return view('admin.pages.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {
        $data = Friend::find($id);
        $data->update($request->all());
        return redirect(route('friends.index'))->with('success','Update successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Friend::where('id',$id)->delete();
        return redirect()->back()->with('success','Delete successful');
    }
    // public function cheek(){
    //     $data = Friend::with('girlfirend')->where('name','shohel')->first();
    //     return view('profile',compact('data'));
    // }
    public function one(){
        $data = girlfirend::with('boyfirend')->find(4);
        return view('profile',compact('data'));
    }


}
