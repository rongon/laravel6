<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiteControllar extends Controller
{
    public function profile(){
        return view('profile');
    }
    public function home(){
        return view('home');
    }
    public function formHAndel(Request $request){
        $ruls = [
            'name' => 'required|min:5',
            'email' => 'required|email'
        ];
        $this->validate($request,$ruls);
        return $request->all();
    }

    public function welcome(){
        return view('welcome');
    }
    public function contact(){
        return view('contact');
    }
    public function friend(){
        return \App\friend::all();
    }
    public function query(){
        return \App\query::all();
    }
   
    public function contactAuth(Request$request){
        return "Your name is: ".$request->input('name');
    }
    
    public function post($id){
        return "Your id is: ".$id;
    }

    public function friends(){
        $students = DB::table('friends')->get();
        return view('profile', ['students' => $students]);
   }
//     public function index(){
//         $students = DB::table('friends')->get();
//         return view('profile', ['students' => $students]);
//    }
}
