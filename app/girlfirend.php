<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class girlfirend extends Model
{
    public function boyfirend(){
        return $this->HasOne('App\Friend','name','boyFriend');
    }
}
