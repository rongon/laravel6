<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('admin/friends','FriendsController');

// Route::get('/','SiteControllar@welcome');
// Route::get('/home','SiteControllar@home');
// Route::get('/friend','SiteControllar@friend');
// Route::get('/friends','SiteControllar@query');
// Route::post('/form','SiteControllar@formHAndel');
Route::get('/profile','FriendsController@one');
// Route::get('/contact','SiteControllar@contact');
// Route::post('/contact','SiteControllar@contactAuth');
// Route::get('/post/{id}','SiteControllar@post');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

