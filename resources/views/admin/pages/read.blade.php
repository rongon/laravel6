@extends('master')
@section('title')
view
@endsection
@section('contain')
    <div class="container">
        <div class="row">
            <div class="card offset-1 col-md-5">
                <div class="card-body">
                    <table class=" table table-striped">
                        <tbody>
                            <tr>
                                <th >Name</th>
                                <td>{{$user->name}}</td>
                            </tr>
                            <tr>
                                <th >Email</th>
                                <td>{{$user->email}}</td>
                            </tr>
                            <tr>
                                <th >Phone</th>
                                <td>{{$user->phone}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card col-md-5">
                <div class="card-body">
                    <table class=" table table-striped">
                        <tbody>
                            <tr>
                                <th >GirlFirnds</th>
                            </tr>
                            @foreach ($user->girlfirend as $girl)
                            <tr>
                                <td>{{$girl->name}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
