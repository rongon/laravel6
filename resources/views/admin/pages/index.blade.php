@extends('master')

@section('title')
friends index
@endsection

@section('contain')
    <div class="container">
        <div class="card">
        <div class="card-body">
        <h2>Data list</h2>
        <hr>
        <table class="table table-striped ">
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Actions</th>
        </tr>


        @foreach($students as $row)
            <tr>
                <td>{{$row->name}}</td>
                <td>{{$row->email}}</td>
                <td>{{$row->phone}}</td>
                <td class="d-flex justify-content">
                    <a href="{{route('friends.show',$row->id)}}" type="button" class="btn btn-primary mr-1">View</a>
                    <a href="{{route('friends.edit',$row->id)}}" type="button" class="btn btn-secondary mr-1">Edit</a>
                    <form action="{{route('friends.destroy',$row->id)}}" method="post">
                        @csrf
                        @method('Delete')
                        <button class="btn btn-danger " type="submit">Delete</button>
                    </form>

                </td>
            </tr>
        @endforeach
        {{$students->links()}}
   </table>
        </div>
   </div>
    </div>

@endsection
