<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title') | laravel6</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body style="background:#EcEcec">
    <section class="bannar ">
    <nav class="navbar navbar-expand-lg navbar-light bg-primary ">
    <div class="container">
            <a class="navbar-brand" href="#">laravel6</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse float-right  " id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

            </ul>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link font-weight-bold" href="{{route('friends.index')}}">List <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link font-weight-bold " href="{{route('friends.create')}}">insart <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
    </section>
    @if(Session::has('success'))
        <h4 class='text-center' style='color:green;'>{{Session::get('success')}}</h4>
    @endif
    <section class="main_contain">
        @yield('contain')
    </section>

    <hr>
    <section class="footer">
    <nav class="nav nav-pills nav-justified container">
        <h4 class="text-center" >This is just learing perpas web with laravel</h4>
    </nav>
    </section>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
