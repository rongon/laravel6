
@extends('master')

@section('title')
form
@endsection

@section('contain')
    <div class="form">
        <center>
        <h2>Contact</h2>
        <form action="{{url('/form')}}" method="post">
            @csrf
            <input type="text" name="name" placeholder="Name"><br><br>
            <input type="email" name="email" placeholder="Email"><br><br>
            <input type="submit" >
        </form>
        </center>
    </div>
@endsection
